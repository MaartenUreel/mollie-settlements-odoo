import xmlrpc.client

TEST = False


class OdooHelper:
    common: xmlrpc.client.ServerProxy
    uid: object
    models: xmlrpc.client.ServerProxy

    def __init__(self, url: str, db: str, user: str, api_key: str):
        self._db = db
        self._api_key = api_key

        self._common = xmlrpc.client.ServerProxy(f"{url}/xmlrpc/2/common")
        self._uid = self._common.authenticate(db, user, api_key, {})
        self._models = xmlrpc.client.ServerProxy(f"{url}/xmlrpc/2/object")

    def execute_kw(self, model: str, method: str, args: list):
        return self._models.execute_kw(self._db, self._uid, self._api_key, model, method, args)

    def record_exists(self, model: str, domain: list) -> bool:
        return self.execute_kw(model, "search_count", [domain]) > 0

    def create_record(self, model: str, data: dict) -> int:
        if TEST:
            return None

        return self.execute_kw(model, "create", [[data]])

    def create_records(self, model: str, data: list[dict]) -> list[int]:
        if TEST:
            return None

        return self.execute_kw(model, "create", [data])

    def search_records(self, model: str, domain: list) -> list[int]:
        return self.execute_kw(model, "search", [domain])

    def read_records(self, model: str, domain: list, fields: list) -> list:
        return self.execute_kw(model, "search_read", [domain], {"fields": fields})

    def delete_record(self, model: str, id: int):
        return self.execute_kw(model, "unlink", [[id]])

    def delete_records(self, model: str, ids: list):
        return self.execute_kw(model, "unlink", [ids])
