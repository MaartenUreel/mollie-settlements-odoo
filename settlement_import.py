import argparse
from decimal import Decimal
from time import sleep

import yaml
from mollie.api.client import Client as MollieClient
from mollie.api.objects.payment import Payment
from mollie.api.objects.settlement import Settlement

from helpers.odoo import OdooHelper
from datetime import datetime


class MollieStatementImport:
    def __init__(self, config_file: str):
        # Read config file
        self._config = self._read_config(config_file)

        # Set up connection to Odoo
        self._odoo = OdooHelper(
            self._config["odoo"]["url"],
            self._config["odoo"]["db"],
            self._config["odoo"]["user"],
            self._config["odoo"]["api_key"],
        )

        # Set up connection to Mollie
        self._mollie = MollieClient()
        self._mollie.set_access_token(self._config["mollie"]["api_key"])

    def _read_config(self, config_file: str):
        # Read yaml
        with open(config_file, "r") as f:
            config = yaml.safe_load(f)

        return config

    def _statement_exists(self, settlement_id: str) -> bool:
        # Check if this statement exists by external reference in Odoo
        return self._odoo.record_exists(
            "account.bank.statement", [["reference", "=", settlement_id]]
        )

    def _payment_to_statement_line(self, payment: Payment) -> dict:
        if payment.status != Payment.STATUS_PAID:
            print(f"Payment {payment.id} is {payment.status}, skipping")
            return None

        line = {
            "id": payment.id,
            "unique_import_id": payment.id,
            "payment_ref": payment.description,
            "date": payment.created_at,
            "amount": payment.amount["value"],
            "journal_id": self._config["odoo"]["mollie_journal_id"],
        }

        print(f"Created statement line for payment {payment.id}")

        return line

    def _create_statement(self, settlement: Settlement) -> int:
        """
        Create a bank statement in Odoo.
        """

        journal_id = self._config["odoo"]["mollie_journal_id"]

        lines = []

        payments = settlement.payments.list()
        for p in payments:
            assert isinstance(p, Payment)

            lines.append(self._payment_to_statement_line(p))

        while payments.has_next():
            payments = payments.get_next()

            for p in payments:
                assert isinstance(p, Payment)
                lines.append(self._payment_to_statement_line(p))

        # Create lines for all the costs
        for year, months in settlement.periods.items():
            for month, period in months.items():
                # Calculate how much costs were witheld from the revenue for the related invoice
                total_costs = sum(
                    Decimal(cost["amountGross"]["value"]) for cost in period["costs"]
                )

                unique_id = f"{settlement.id}__{period['invoiceReference']}"
                lines.append(
                    {
                        "id": unique_id,
                        "unique_import_id": unique_id,
                        "payment_ref": period["invoiceReference"],
                        "date": settlement.settled_at,
                        "amount": str(total_costs * -1),
                        # "statement_id": statement_id,
                        "journal_id": journal_id,
                    }
                )

                print(f"Created statement line for costs {settlement.id}")

        # Create lines for the payout itself (settlement)
        lines.append(
            {
                "id": settlement.id,
                "unique_import_id": settlement.id,
                "payment_ref": f"Settlement {settlement.reference}",
                "date": settlement.settled_at,
                "amount": str(Decimal(settlement.amount["value"]) * -1),
                # "statement_id": statement_id,
                "journal_id": self._config["odoo"]["mollie_journal_id"],
            }
        )

        # Create lines in bulk
        lines = self._odoo.create_records("account.bank.statement.line", lines)

        print(f"Created statement line for settlement {settlement.id}")

        # Create statement
        statement_id = self._odoo.create_record(
            "account.bank.statement",
            {
                "name": settlement.reference,
                "date": settlement.settled_at,
                "create_date": settlement.created_at,
                "reference": settlement.id,
                "journal_id": journal_id,
                "line_ids": [(6, 0, lines)],
                # "state": "draft",
            },
        )

        return statement_id

    def _settlement_date_valid(self, settlement: Settlement) -> bool:
        """
        Check if the statement date is valid.
        """

        # Parse the optional ignore_before parameter
        if ignore_before := self._config["mollie"].get("ignore_before"):
            # The YAML file will give us date (not datetime)
            statement_date = datetime.fromisoformat(settlement.settled_at).date()

            return statement_date >= ignore_before

        # If no ignore_before is set, the statement is always valid
        return True

    def run_import(self):
        # Parse the optional ignore_before parameter
        ignore_before = self._config["mollie"].get("ignore_before")

        # Get all mollie statements.
        mollie_settlements = self._mollie.settlements.list()

        for settlement in mollie_settlements:
            # Check if statement is already imported
            if self._statement_exists(settlement.id):
                print(f"Statement {settlement.id} already exists, skipping")
                continue

            # Check if the statement is before the ignore_before date
            if not self._settlement_date_valid(settlement):
                print(
                    f"Statement {settlement.id} is before the ignore_before date, skipping"
                )
                continue

            # Create statement
            self._create_statement(settlement)

            # Sleep for 2 seconds to prevent rate limiting
            sleep(2)


if __name__ == "__main__":
    # First optional argument is the path to the config file
    args = argparse.ArgumentParser(description="Import Mollie settlements to Odoo")
    args.add_argument(
        "-c",
        "--config",
        help="Specify a config file",
        default="config.yml",
    )
    args = args.parse_args()

    m = MollieStatementImport(args.config)
    m.run_import()
