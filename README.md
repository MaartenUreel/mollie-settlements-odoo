# Mollie Settlements to Odoo

## Introduction
This project was created to automate the import of settelements from Mollie into our Odoo environments.

There is an existing implementation using MyPonto for this, however this has a number of issues.

* The transaction list does not include the witheld fees (payment of the Mollie invoices)
* Odoo retrieves all the transactions after the last retrieval, but when the payment processing was delayed (e.g. in the case of wire transfer),
  this transaction is not captured in Odoo.
* We only use Mollie for 2-3 months per year but need to pay monthly for the connection regardless.

## Usage
The implementation is very simple.

- Create an Organization Access Token at Mollie with the permissions: `settlements.read` and `payments.read`
- Create an API token in Odoo on a user with permissions to import bank statements etc
- Create a file config.yml (you can refer to the sample file)
- Run settlement_import.py

If you want to have multiple configurations, create multiple files and call the script passing the configuration file as `--config` parameter:
```
settlement_import.py --config=some_file.yml
```

settlement_import.py
